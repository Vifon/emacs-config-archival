(set-register ?p `(file . ,(concat org-directory "/projects.org")))
(set-register ?k `(file . ,org-default-notes-file))
(set-register ?j `(file . ,(concat org-directory "/journal.org")))

(provide 'my-registers)
