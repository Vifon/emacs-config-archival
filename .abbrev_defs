;;-*-coding: emacs-mule;mode: emacs-lisp-*-
(define-abbrev-table 'Buffer-menu-mode-abbrev-table '(    ))

(define-abbrev-table 'awk-mode-abbrev-table '(    ))

(define-abbrev-table 'c++-mode-abbrev-table '(
                                              ("inc" "#include " nil 1)
                                              ("ccout" "std::cout" nil 0)
                                              ("ccerr" "std::cerr" nil 0)
                                              ("ccin" "std::cin" nil 0)
                                              ("eendl" "std::endl" nil 0)
                                              ))

(define-abbrev-table 'c-mode-abbrev-table '(
                                            ("inc" "#include " nil 1)))

(define-abbrev-table 'comint-mode-abbrev-table '(    ))

(define-abbrev-table 'completion-list-mode-abbrev-table '(    ))

(define-abbrev-table 'fundamental-mode-abbrev-table '(    ))

(define-abbrev-table 'global-abbrev-table '(    ))

(define-abbrev-table 'idl-mode-abbrev-table '(    ))

(define-abbrev-table 'java-mode-abbrev-table '(    ))

(define-abbrev-table 'lisp-mode-abbrev-table '(    ))

(define-abbrev-table 'makefile-automake-mode-abbrev-table '(    ))

(define-abbrev-table 'makefile-bsdmake-mode-abbrev-table '(    ))

(define-abbrev-table 'makefile-gmake-mode-abbrev-table '(    ))

(define-abbrev-table 'makefile-imake-mode-abbrev-table '(    ))

(define-abbrev-table 'makefile-makepp-mode-abbrev-table '(    ))

(define-abbrev-table 'makefile-mode-abbrev-table '(    ))

(define-abbrev-table 'objc-mode-abbrev-table '(    ))

(define-abbrev-table 'pike-mode-abbrev-table '(    ))

(define-abbrev-table 'sh-mode-abbrev-table '(    ))

(define-abbrev-table 'special-mode-abbrev-table '(    ))

(define-abbrev-table 'text-mode-abbrev-table '(    ))

(define-abbrev-table 'vc-git-log-view-mode-abbrev-table '(    ))

